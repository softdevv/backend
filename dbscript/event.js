const mongoose = require('mongoose')
const Event = require('../models/event')
mongoose.connect('mongodb://localhost:27017/example')
async function clear () {
  await Event.deleteMany({})
}

async function main () {
  await clear()
  await Event.insertMany([
    {
      title: 'นาย รมย์รวินต์ อินไชย', content: 'IF301-01', startDate: new Date('2022-04-17 09:00'), endDate: new Date('2022-04-17 12:00'), class: 'b'
    },
    {
      title: 'นาย ธนา เสมอวงษ์', content: 'IF301-02', startDate: new Date('2022-04-29 08:00'), endDate: new Date('2022-04-29 12:00'), class: 'a'
    },
    {
      title: 'นาย นิพิฐพนธ์ ปะทีปะวณิช', content: 'IF301-03', startDate: new Date('2022-04-18 10:00'), endDate: new Date('2022-04-18 13:00'), class: 'c'
    },
    {
      title: 'นาย สพลเชษฐ์ วารกิจสถาพร', content: 'IF301-04', startDate: new Date('2022-04-20 09:00'), endDate: new Date('2022-04-20 10:00'), class: 'b'
    },
    {
      title: 'นาย สมชาย มาดี', content: 'IF301-05', startDate: new Date('2022-04-23 13:00'), endDate: new Date('2022-04-23 17:00'), class: 'c'
    }
  ])
  Event.find({})
}

main().then(function () {
  console.log('Finish')
})
